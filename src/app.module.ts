import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { GroupDetailsModule } from './group_details/group_details.module';
import { Group_Detail } from './group_details/group_details.entity';
import { GroupMembersModule } from './group_members/group_members.module';
import { GroupTypesModule } from './group_types/group_types.module';
import { GroupPendingApprovalModule } from './group_pending_approval/group_pending_approval.module';
import { Group_Member } from './group_members/group_members.entity';
import { Group_Type } from './group_types/group_types.entity';
import { Group_Pending_Approval } from './group_pending_approval/group_pending_approval.entity';
import { RolesReferenceTableModule } from './roles_reference_table/roles_reference_table.module';
import { Roles_Reference_Table } from './roles_reference_table/roles_reference_table.entity';
import { Group_Feed_Details } from './group_feed_details/group_feed_details.entity';
import { GroupFeedDetailsModule } from './group_feed_details/group_feed_details.module';

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'ManuTanu@1234',
      database: 'dbstructure',
      models: [
        Group_Detail,
        Group_Member,
        Group_Type,
        Group_Pending_Approval,
        Roles_Reference_Table,
        Group_Feed_Details,
      ],
      synchronize: true,
      autoLoadModels: true,
    }),

    GroupDetailsModule,
    GroupMembersModule,
    GroupTypesModule,
    GroupPendingApprovalModule,
    RolesReferenceTableModule,
    GroupFeedDetailsModule,
  ],
})
export class AppModule {}
