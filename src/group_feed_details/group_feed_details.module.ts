import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Group_Feed_Details } from './group_feed_details.entity';
import { GroupFeedDetailsController } from './group_feed_details.controller';
import { GroupFeedDetailsService } from './group_feed_details.service';

@Module({
  imports: [SequelizeModule.forFeature([Group_Feed_Details])],
  controllers: [GroupFeedDetailsController],
  providers: [GroupFeedDetailsService],
})
export class GroupFeedDetailsModule {}
