import { Injectable, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Group_Feed_Details } from './group_feed_details.entity';
import { group } from 'console';

@Injectable()
export class GroupFeedDetailsService {
  constructor(
    @InjectModel(Group_Feed_Details)
    private feedRepository: typeof Group_Feed_Details,
  ) {}

  async create(groupFeed: Group_Feed_Details): Promise<Group_Feed_Details> {
    console.log(groupFeed);
    return await this.feedRepository.create(groupFeed);
  }

  async findByGroupName(groupId: string) {
    const { QueryTypes } = require('sequelize');
    groupId = groupId.replace(':', '');

    const found = await this.feedRepository.sequelize.query(
      "select gf.daysago, gf.hoursago, gf.groupFeedText, concat(users.firstName,' ',users.lastName) as 'memberName' from (select *, DATEDIFF(sysdate(), createdAt) as 'daysago', HOUR(TIMEDIFF(sysdate(), createdAt)) as 'hoursago' from group_feed_details where groupID= :name order by (sysdate()-createdAt) asc) gf, users where gf.memberID = users.userId",
      {
        replacements: { name: groupId },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }
}
