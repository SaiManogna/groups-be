import { Body, Controller, Get, Param, Post, Delete } from '@nestjs/common';
import { Group_Feed_Details } from './group_feed_details.entity';
import { GroupFeedDetailsService } from './group_feed_details.service';

@Controller('groupfeed')
export class GroupFeedDetailsController {
  constructor(private groupFeedDetailsService: GroupFeedDetailsService) {}

  @Post()
  async create(
    @Body() groupFeedDetails: Group_Feed_Details,
  ): Promise<Group_Feed_Details> {
    return this.groupFeedDetailsService.create(groupFeedDetails);
  }

  @Get('/findByGroupName:groupId')
  async findByGroupName(@Param('groupId') groupId) {
    return this.groupFeedDetailsService.findByGroupName(groupId);
  }
}
