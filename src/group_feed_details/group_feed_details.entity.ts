import { Table, Column, Model, DataType, Length } from 'sequelize-typescript';

@Table
export class Group_Feed_Details extends Model<Group_Feed_Details> {
  @Column({
    primaryKey: true,
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    // type: DataType.UUID,
    comment: 'Serial Number',
  })
  groupFeedId: string;

  @Column({
    type: DataType.TEXT,
    //primaryKey: true,
  })
  groupID: string;

  @Column({
    type: DataType.TEXT,
    //primaryKey: true,
  })
  memberID: string;

  @Column({
    type: DataType.TEXT,
    comment: 'Feed Details',
  })
  groupFeedText: string;
}
