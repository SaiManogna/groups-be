import {
  Table,
  Column,
  Model,
  DataType,
  Default,
  Length,
  HasMany,
} from 'sequelize-typescript';

@Table
export class Group_Type extends Model<Group_Type> {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    //unique: true,
    comment: 'Serial Number',
  })
  groupTypeID: number;

  @Length({ max: 10 })
  @Column({
    //type: DataType.TEXT,
    unique: true,
    comment: 'Club or Class or Group',
  })
  groupType: string;
}
