import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { Group_Type } from './group_types.entity';
import { GroupTypesService } from './group_types.service';

@Controller('grouptypes')
export class GroupTypesController {
  constructor(private groupTypesService: GroupTypesService) {}

  @Post()
  async create(@Body() groupTypes: Group_Type): Promise<Group_Type> {
    return this.groupTypesService.create(groupTypes);
  }

  @Get()
  async findAll(): Promise<Group_Type[]> {
    return this.groupTypesService.findAll();
  }

  @Get('/findGroupTypes')
  async findGroupTypes() {
    return this.groupTypesService.findGroupTypes();
  }
}
