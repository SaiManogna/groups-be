import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Group_Type } from './group_types.entity';
import { GroupTypesController } from './group_types.controller';
import { GroupTypesService } from './group_types.service';

@Module({
  imports: [SequelizeModule.forFeature([Group_Type])],
  controllers: [GroupTypesController],
  providers: [GroupTypesService],
})
export class GroupTypesModule {}
