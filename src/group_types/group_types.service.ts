import { Injectable, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Group_Type } from './group_types.entity';

@Injectable()
export class GroupTypesService {
  constructor(
    @InjectModel(Group_Type)
    private groupTypeRepository: typeof Group_Type,
  ) {}

  async create(groupTypes: Group_Type): Promise<Group_Type> {
    return await this.groupTypeRepository.create(groupTypes);
  }

  async findAll(): Promise<Group_Type[]> {
    return await this.groupTypeRepository.findAll();
  }

  async findGroupTypes() {
    const { QueryTypes } = require('sequelize');

    const found = await this.groupTypeRepository.sequelize.query(
      'select distinct groupType from group_types',
      {
        type: QueryTypes.SELECT,
      },
    );

    console.log(found);
    return found;
  }
}
