import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Roles_Reference_Table } from './roles_reference_table.entity';
import { RolesReferenceTableController } from './roles_reference_table.controller';
import { RolesReferenceTableService } from './roles_reference_table.service';

@Module({
  imports: [SequelizeModule.forFeature([Roles_Reference_Table])],
  controllers: [RolesReferenceTableController],
  providers: [RolesReferenceTableService],
})
export class RolesReferenceTableModule {}
