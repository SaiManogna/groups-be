import { Injectable, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Roles_Reference_Table } from './roles_reference_table.entity';

@Injectable()
export class RolesReferenceTableService {
  constructor(
    @InjectModel(Roles_Reference_Table)
    private rolesRepository: typeof Roles_Reference_Table,
  ) {}

  async create(
    rolesReferenceTable: Roles_Reference_Table,
  ): Promise<Roles_Reference_Table> {
    return await this.rolesRepository.create(rolesReferenceTable);
  }

  async findRoles() {
    const { QueryTypes } = require('sequelize');

    const found = await this.rolesRepository.sequelize.query(
      'select roleName as "label", roleValue as "value" from roles_reference_tables',
      {
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }
}
