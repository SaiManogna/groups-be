import {
  Table,
  Column,
  Model,
  DataType,
  Length,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

@Table
export class Roles_Reference_Table extends Model<Roles_Reference_Table> {
  @Length({ max: 10 })
  @Column({
    primaryKey: true,
    //type: DataType.TEXT,
    comment: 'Name of Role',
  })
  roleName: string;

  @Length({ max: 10 })
  @Column({
    type: DataType.TEXT,
    comment: 'Value of Role',
  })
  roleValue: string;
}
