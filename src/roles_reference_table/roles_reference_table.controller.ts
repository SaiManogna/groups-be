import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { Roles_Reference_Table } from './roles_reference_table.entity';
import { RolesReferenceTableService } from './roles_reference_table.service';

@Controller('roles')
export class RolesReferenceTableController {
  constructor(private rolesReferenceTableService: RolesReferenceTableService) {}

  @Post()
  async create(
    @Body() rolesReferenceTable: Roles_Reference_Table,
  ): Promise<Roles_Reference_Table> {
    return this.rolesReferenceTableService.create(rolesReferenceTable);
  }

  /**@Get(':groupName')
  async findByGroupName(@Param('groupName') groupName): Promise<Group_Member> {
    return this.groupMembersService.findByGroupName(groupName);
  }*/

  @Get()
  async findRoles() {
    return this.rolesReferenceTableService.findRoles();
  }

  /** 
  @Get()
  async findAll(): Promise<Group_Member[]> {
    return this.groupMembersService.findAll();
  }*/
}
