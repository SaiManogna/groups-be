import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Delete,
  UseInterceptors,
  UploadedFile,
  Res,
  Put,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Group_Detail } from './group_details.entity';
import { diskStorage } from 'multer';
import { GroupDetailsService } from './group_details.service';
import { extname } from 'path';

@Controller('groups')
export class GroupDetailsController {
  SERVER_URL: string = 'http://localhost:5000/groups/';
  constructor(private groupDetailsService: GroupDetailsService) {}

  @Post()
  async create(@Body() groupDetail: Group_Detail): Promise<Group_Detail> {
    return this.groupDetailsService.create(groupDetail);
  }

  @Put('/updateGroupDetails:combinedString')
  async updateGroupDetails(@Param('combinedString') combinedString) {
    return this.groupDetailsService.updateGroupDetails(combinedString);
  }

  @Get('/getByGroupName:groupName')
  async find(@Param('groupName') groupName) {
    return this.groupDetailsService.findByGroupName(groupName);
  }

  @Get('/findByGroupID:groupID')
  async findByGroupID(@Param('groupID') groupID) {
    return this.groupDetailsService.findByGroupID(groupID);
  }

  @Get('/getGroupID:groupNamegroupType')
  async findGroupID(@Param('groupNamegroupType') groupNamegroupType) {
    return this.groupDetailsService.findGroupID(groupNamegroupType);
  }

  @Get()
  async findAll(): Promise<Group_Detail[]> {
    return this.groupDetailsService.findAll();
  }

  @Delete('/deleteGroup:groupID')
  async delete(@Param('groupID') groupID) {
    return this.groupDetailsService.deleteGroup(groupID);
  }

  @Post('/upload/groupCoverPic')
  @UseInterceptors(FileInterceptor('file'))
  async uploadgroupCoverPic(@UploadedFile() file) {
    console.log('hello');
    console.log(file);
    //return `${this.SERVER_URL}groupCoverPic/${file.filename}`;
  }

  @Post('groupCoverPic/default')
  async defaultGroupCoverPic(): Promise<any> {
    return `${this.SERVER_URL}groupCoverPic/default.png`;
  }

  @Get('groupCoverPic/:fileId')
  async servegroupCoverPic(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'images/groupCoverPic' });
  }

  /** To be added in users table */
  @Get('/getUserDetails')
  async getUserDetails() {
    return this.groupDetailsService.getUserDetails();
  }
}
