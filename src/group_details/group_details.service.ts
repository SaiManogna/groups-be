import { Injectable, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Group_Detail } from './group_details.entity';

@Injectable()
export class GroupDetailsService {
  constructor(
    @InjectModel(Group_Detail)
    private groupRepository: typeof Group_Detail,
  ) {}

  async create(groupDetail: Group_Detail): Promise<Group_Detail> {
    return await this.groupRepository.create(groupDetail);
  }

  async updateGroupDetails(combinedString: string) {
    let params = combinedString.split(':');
    const { QueryTypes } = require('sequelize');

    const found = await this.groupRepository.sequelize.query(
      'update group_details set groupName= :name, groupType= :type, groupDescription= :description, groupPrivacy= :privacy, groupLocation= :location where groupID= :group',
      {
        replacements: {
          group: params[1],
          name: params[3],
          type: params[2],
          description: params[4],
          privacy: params[5],
          location: params[6],
        },
        type: QueryTypes.UPDATE,
      },
    );

    return found;
  }

  async findByGroupName(groupName: string) {
    groupName = groupName.replace(':', '');
    const { QueryTypes } = require('sequelize');

    const found = await this.groupRepository.sequelize.query(
      "select groupID, groupName, groupType, groupDescription, groupPrivacy, groupLocation, groupCoverPic, groupProfilePic, createdByEmailID, DATE_FORMAT(createdAt, '%d %b %Y') as createdAt, updatedAt from group_details where groupName = :name",
      {
        replacements: { name: groupName },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async findByGroupID(groupID: string) {
    groupID = groupID.replace(':', '');
    const { QueryTypes } = require('sequelize');

    const found = await this.groupRepository.sequelize.query(
      "select groupID, groupName, groupType, groupDescription, groupPrivacy, groupLocation, groupCoverPic, groupProfilePic, createdByEmailID, DATE_FORMAT(createdAt, '%d %b %Y') as createdAt, updatedAt from group_details where groupID = :group",
      {
        replacements: { group: groupID },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async findGroupID(groupNamegroupType: string) {
    let params = groupNamegroupType.split(':');
    const { QueryTypes } = require('sequelize');

    const found = await this.groupRepository.sequelize.query(
      'select groupID from group_details where groupName = :name and groupType =:type',
      {
        replacements: { name: params[1], type: params[2] },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async findAll() {
    return await this.groupRepository.findAll();
  }

  async deleteGroup(groupID: string) {
    groupID = groupID.replace(':', '');
    return await this.groupRepository.destroy({
      where: { groupID: groupID },
    });
  }

  /** To be added in users table */
  async getUserDetails() {
    const { QueryTypes } = require('sequelize');

    const found = await this.groupRepository.sequelize.query(
      "select *, concat(firstName,' ',lastName) as 'name', false as 'isChecked' from users",
      {
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }
}
