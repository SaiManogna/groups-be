import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Group_Detail } from './group_details.entity';
import { GroupDetailsController } from './group_details.controller';
import { GroupDetailsService } from './group_details.service';

@Module({
  imports: [SequelizeModule.forFeature([Group_Detail])],
  controllers: [GroupDetailsController],
  providers: [GroupDetailsService],
})
export class GroupDetailsModule {}
