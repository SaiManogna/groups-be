import {
  Table,
  Column,
  Model,
  DataType,
  Default,
  Length,
} from 'sequelize-typescript';

@Table
export class Group_Detail extends Model<Group_Detail> {
  @Column({
    // autoIncrement: true,
    // primaryKey: true,
    // type: DataType.UUID,
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    comment: 'Serial Number',
  })
  groupID: string;

  @Length({ max: 50 })
  @Column({
    primaryKey: true,
    //type: DataType.TEXT,
    comment: 'Name of Group',
  })
  groupName: string;

  @Length({ max: 10 })
  @Column({
    primaryKey: true,
    //type: DataType.TEXT,
    comment: 'Club or Class or Group',
  })
  groupType: string;

  @Length({ max: 200 })
  @Column({
    type: DataType.TEXT,
    comment: 'About the group',
  })
  groupDescription: string;

  @Length({ max: 10 })
  @Default({ value: 'Public' })
  @Column({
    type: DataType.TEXT,
    comment: 'Public or Private',
  })
  groupPrivacy: string;

  @Length({ max: 50 })
  @Column({
    type: DataType.TEXT,
    comment: 'Optional Field',
  })
  groupLocation: string;

  @Column({
    type: DataType.TEXT,
    comment: 'URI of the Cover Image',
  })
  groupCoverPic: string;

  @Column({
    type: DataType.TEXT,
    comment: 'URI of the Profile Image',
  })
  groupProfilePic: string;

  @Column({
    type: DataType.TEXT,
    comment: 'Created By',
  })
  createdByEmailID: string;
}
