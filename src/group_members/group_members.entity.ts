import { Table, Column, Model, DataType, Length } from 'sequelize-typescript';

@Table
export class Group_Member extends Model<Group_Member> {
  @Column({
    //primaryKey: true,
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    // type: DataType.UUID,
    comment: 'Serial Number',
  })
  groupMemberID: string;

  @Column({
    //type: DataType.TEXT,
    primaryKey: true,
  })
  groupID: string;

  @Column({
    //type: DataType.TEXT,
    primaryKey: true,
  })
  memberID: string;

  @Length({ max: 25 })
  @Column({
    type: DataType.TEXT,
    comment: 'Role of the member',
  })
  memberDesignation: string;
}
