import { Injectable, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Group_Member } from './group_members.entity';

@Injectable()
export class GroupMembersService {
  constructor(
    @InjectModel(Group_Member)
    private memberRepository: typeof Group_Member,
  ) {}

  async create(groupMember: Group_Member): Promise<Group_Member> {
    console.log(groupMember);
    return await this.memberRepository.create(groupMember);
  }

  async findByGroupName(groupID: string) {
    const { QueryTypes } = require('sequelize');
    groupID = groupID.replace(':', '');

    const found = await this.memberRepository.sequelize.query(
      "select gm.*, concat(users.firstName,' ', users.lastName) as 'memberName' from (select * from group_members where groupID= :group and memberDesignation <> :status) gm, users where gm.memberID = users.userId",
      {
        replacements: { group: groupID, status: 'Member' },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async getMembersOfGroup(groupID: string) {
    const { QueryTypes } = require('sequelize');
    groupID = groupID.replace(':', '');

    const found = await this.memberRepository.sequelize.query(
      'select * from group_members where groupID= :group and memberDesignation = :status',
      {
        replacements: { group: groupID, status: 'Member' },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async findByMemberName(memberName: string): Promise<Group_Member> {
    memberName = memberName.replace(':', '');
    const result = await this.memberRepository.findOne<Group_Member>({
      where: {
        memberName: memberName,
      },
    });

    return result;
  }

  async getMemberDetails(userID: string): Promise<Group_Member> {
    userID = userID.replace(':', '');
    const result = await this.memberRepository.findOne<Group_Member>({
      where: {
        memberID: userID,
      },
    });

    return result;
  }

  async findAll(): Promise<Group_Member[]> {
    return await this.memberRepository.findAll();
  }

  async findMyGroups(userID: string) {
    const { QueryTypes } = require('sequelize');
    userID = userID.replace(':', '');

    const found = await this.memberRepository.sequelize.query(
      "select gd.groupID, gm.countMembers, gm.admin, gd.groupType, gd.groupName as 'name', gd.groupProfilePic from (Select distinct groupID as 'group', count(distinct memberID) as 'countMembers', 'Y' as admin from group_members where groupID in (Select distinct groupID from group_members where memberID= :name and memberDesignation= :adminStatus) group by groupID union  Select distinct groupID as 'group', count(distinct memberID) as 'countMembers', 'N' as admin from group_members where groupID in (Select distinct groupID from group_members where memberID= :name and memberDesignation<> :adminStatus) group by groupID ) gm, group_details gd where gm.group = gd.groupID",
      {
        replacements: { name: userID, adminStatus: 'Admin' },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async findExploreGroups(userID: string) {
    const { QueryTypes } = require('sequelize');
    userID = userID.replace(':', '');

    const found = await this.memberRepository.sequelize.query(
      'select distinct gd.groupID, gd.groupName, gd.groupType, gm.countMembers, gd.groupPrivacy, gd.groupProfilePic from (SELECT distinct groupID, count(*) as countMembers FROM group_members WHERE groupID NOT IN (SELECT distinct groupID FROM group_members WHERE memberID = :name) group by groupID) gm, group_details gd where gm.groupID = gd.groupID',
      {
        replacements: { name: userID },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async findGroupMembersCount(groupID: string) {
    const { QueryTypes } = require('sequelize');
    groupID = groupID.replace(':', '');

    const found = await this.memberRepository.sequelize.query(
      'select groupID, count(*) as countMembers from group_members where groupID = :name group by groupID',
      {
        replacements: { name: groupID },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async findIfMember(groupNamegroupTypememberEmailId: string) {
    const { QueryTypes } = require('sequelize');
    console.log(groupNamegroupTypememberEmailId);
    let params = groupNamegroupTypememberEmailId.split(':');

    const found = await this.memberRepository.sequelize.query(
      'select * from group_members where groupID= :group and memberID= :member',
      {
        replacements: { group: params[1], member: params[2] },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async deleteGroup(groupID: string) {
    groupID = groupID.replace(':', '');
    return await this.memberRepository.destroy({
      where: { groupID: groupID },
    });
  }

  async deleteRoleFromGroup(combinedString: string) {
    let params = combinedString.split(':');
    return await this.memberRepository.destroy({
      where: {
        groupID: params[2],
        memberDesignation: params[3],
        memberID: params[1],
      },
    });
  }

  async leaveGroup(combinedString: string) {
    let params = combinedString.split(':');
    return await this.memberRepository.destroy({
      where: {
        groupID: params[2],
        memberID: params[1],
      },
    });
  }
}
