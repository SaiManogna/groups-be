import { Body, Controller, Get, Param, Post, Delete } from '@nestjs/common';
import { Group_Member } from './group_members.entity';
import { GroupMembersService } from './group_members.service';

@Controller('members')
export class GroupMembersController {
  constructor(private groupMembersService: GroupMembersService) {}

  @Post()
  async create(@Body() groupMember: Group_Member): Promise<Group_Member> {
    return this.groupMembersService.create(groupMember);
  }

  @Get('/findByGroupName:groupID')
  async findByGroupName(@Param('groupID') groupID) {
    return this.groupMembersService.findByGroupName(groupID);
  }

  @Get('/myGroups:userID')
  async findMyGroups(@Param('userID') userID) {
    return this.groupMembersService.findMyGroups(userID);
  }

  @Get('/getMembersOfGroup:groupID')
  async getMembersOfGroup(@Param('groupID') groupID) {
    return this.groupMembersService.getMembersOfGroup(groupID);
  }

  @Get('/getMemberDetails:userID')
  async getMemberDetails(@Param('userID') userID) {
    return this.groupMembersService.getMemberDetails(userID);
  }

  @Get('/exploreGroups:userID')
  async findExploreGroups(@Param('userID') userID) {
    return this.groupMembersService.findExploreGroups(userID);
  }

  @Get('/findGroupMembersCount:groupID')
  async findGroupMembersCount(@Param('groupID') groupID) {
    return this.groupMembersService.findGroupMembersCount(groupID);
  }

  @Get('/findIfMember:groupNamegroupTypememberEmailId')
  async findIfMember(
    @Param('groupNamegroupTypememberEmailId') groupNamegroupTypememberEmailId,
  ) {
    return this.groupMembersService.findIfMember(
      groupNamegroupTypememberEmailId,
    );
  }

  @Delete('/deleteGroup:groupID')
  async delete(@Param('groupID') groupID) {
    return this.groupMembersService.deleteGroup(groupID);
  }

  @Delete('/deleteRoleFromGroup:combinedString')
  async deleteRoleFromGroup(@Param('combinedString') combinedString) {
    return this.groupMembersService.deleteRoleFromGroup(combinedString);
  }

  @Delete('/leaveGroup:combinedString')
  async leaveGroup(@Param('combinedString') combinedString) {
    return this.groupMembersService.leaveGroup(combinedString);
  }
}
