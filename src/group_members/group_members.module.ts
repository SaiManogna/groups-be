import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Group_Member } from './group_members.entity';
import { GroupMembersController } from './group_members.controller';
import { GroupMembersService } from './group_members.service';

@Module({
  imports: [SequelizeModule.forFeature([Group_Member])],
  controllers: [GroupMembersController],
  providers: [GroupMembersService],
})
export class GroupMembersModule {}
