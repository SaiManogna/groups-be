import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Group_Pending_Approval } from './group_pending_approval.entity';
import { GroupPendingApprovalController } from './group_pending_approval.controller';
import { GroupPendingApprovalService } from './group_pending_approval.service';

@Module({
  imports: [SequelizeModule.forFeature([Group_Pending_Approval])],
  controllers: [GroupPendingApprovalController],
  providers: [GroupPendingApprovalService],
})
export class GroupPendingApprovalModule {}
