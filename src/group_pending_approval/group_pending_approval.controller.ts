import { Body, Controller, Get, Param, Post, Delete } from '@nestjs/common';
import { Group_Pending_Approval } from './group_pending_approval.entity';
import { GroupPendingApprovalService } from './group_pending_approval.service';

@Controller('pendingapprovals')
export class GroupPendingApprovalController {
  constructor(
    private groupPendingApprovalService: GroupPendingApprovalService,
  ) {}

  @Post()
  async create(
    @Body() groupPendingApproval: Group_Pending_Approval,
  ): Promise<Group_Pending_Approval> {
    return this.groupPendingApprovalService.create(groupPendingApproval);
  }

  @Get('/myPendingApprovals:memberId')
  async findPendingApprovals(@Param('memberId') memberId) {
    return this.groupPendingApprovalService.findPendingApprovals(memberId);
  }

  @Post(
    '/updatePendingApprovalStatus:memberEmailIdgroupNamegroupTypeapprovalStatus',
  )
  async updatePendingApprovalStatus(
    @Param('memberEmailIdgroupNamegroupTypeapprovalStatus')
    memberEmailIdgroupNamegroupTypeapprovalStatus,
  ) {
    return this.groupPendingApprovalService.updatePendingApprovalStatus(
      memberEmailIdgroupNamegroupTypeapprovalStatus,
    );
  }

  @Get('/findRequestSentStatus:memberEmailIdgroupNamegroupType')
  async findRequestSentStatus(
    @Param('memberEmailIdgroupNamegroupType')
    memberEmailIdgroupNamegroupType,
  ) {
    return this.groupPendingApprovalService.findRequestSentStatus(
      memberEmailIdgroupNamegroupType,
    );
  }

  @Delete('/deleteGroup:groupID')
  async delete(@Param('groupID') groupID) {
    return this.groupPendingApprovalService.deleteGroup(groupID);
  }
}
