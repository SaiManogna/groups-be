import { Table, Column, Model, DataType, Length } from 'sequelize-typescript';

@Table
export class Group_Pending_Approval extends Model<Group_Pending_Approval> {
  @Column({
    //primaryKey: true,
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    // type: DataType.UUID,
    comment: 'Serial Number',
  })
  groupPendingApprovalId: string;

  @Column({
    //type: DataType.TEXT,
    primaryKey: true,
  })
  groupID: string;

  @Column({
    //type: DataType.TEXT,
    primaryKey: true,
  })
  memberID: string;

  @Length({ max: 10 })
  @Column({
    comment: 'Yes (Y) or No (N)',
  })
  Responded: string;

  @Length({ max: 10 })
  @Column({
    comment: 'Approved (A) or Not Approved (NA)',
  })
  approvalStatus: string;
}
