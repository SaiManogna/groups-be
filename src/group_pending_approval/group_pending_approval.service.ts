import { Injectable, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Group_Pending_Approval } from './group_pending_approval.entity';

@Injectable()
export class GroupPendingApprovalService {
  constructor(
    @InjectModel(Group_Pending_Approval)
    private pendingApprovalRepository: typeof Group_Pending_Approval,
  ) {}

  async create(
    pendingApproval: Group_Pending_Approval,
  ): Promise<Group_Pending_Approval> {
    return await this.pendingApprovalRepository.create(pendingApproval);
  }

  async findAll(): Promise<Group_Pending_Approval[]> {
    return await this.pendingApprovalRepository.findAll();
  }

  async findPendingApprovals(memberId: string) {
    const { QueryTypes } = require('sequelize');
    memberId = memberId.replace(':', '');

    const found = await this.pendingApprovalRepository.sequelize.query(
      "select distinct gf.memberID, gf.groupID, concat(users.firstName,' ',users.lastName) as 'memberName', gd.groupName, users.email as 'memberEmailId', gd.groupType from (select groupID, memberID from group_pending_approvals where groupId in (select distinct groupID from group_members where memberDesignation = :adminStatus and memberId = :member) and Responded= :respondStatus) gf, group_details gd, users where gf.groupID = gd.groupID and users.userId = gf.memberID;",
      {
        replacements: {
          member: memberId,
          adminStatus: 'Admin',
          respondStatus: 'N',
        },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async findRequestSentStatus(memberEmailIdgroupNamegroupType: string) {
    const { QueryTypes } = require('sequelize');
    let params = memberEmailIdgroupNamegroupType.split(':');

    const found = await this.pendingApprovalRepository.sequelize.query(
      'select * from group_pending_approvals where memberID = :member and approvalStatus is null and groupID = :group',
      {
        replacements: {
          member: params[1],
          group: params[2],
        },
        type: QueryTypes.SELECT,
      },
    );

    return found;
  }

  async updatePendingApprovalStatus(
    memberEmailIdgroupNamegroupTypeapprovalStatus: string,
  ) {
    const { QueryTypes } = require('sequelize');
    let params = memberEmailIdgroupNamegroupTypeapprovalStatus.split(':');

    const found = await this.pendingApprovalRepository.sequelize.query(
      'select * from group_pending_approvals where memberID = :member and Responded=:respondStatus and groupID= :group',
      {
        replacements: {
          member: params[1],
          respondStatus: 'N',
          group: params[2],
        },
        type: QueryTypes.SELECT,
      },
    );

    if (found) {
      await this.pendingApprovalRepository.update(
        { Responded: 'Y', approvalStatus: params[3] },
        {
          where: {
            memberID: params[1],
            Responded: 'N',
            groupID: params[2],
          },
        },
      );
    }

    return found;
  }

  async deleteGroup(groupID: string) {
    groupID = groupID.replace(':', '');
    return await this.pendingApprovalRepository.destroy({
      where: { groupID: groupID },
    });
  }
}
